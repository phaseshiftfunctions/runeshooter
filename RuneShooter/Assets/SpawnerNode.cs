﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;


public class spawnerNode : waypointNode {
	
	/*
	 * Spawner nodes start and immediately place the desired runes evenly along the path. 
	 * The runes do not get movement scripts: they are static. 
	 * 
	 */
	public int desiredCount = -1; //the number of runes this emits. -1 = infinite.
	public bool emitsPatterns = false;
	public SerializablePattern patternToEmit;
	
	public int count = -1;

	public List<int> pathscript = new List<int>();
	int aspectindex = 0;
	public List<string> aspectscript = new List<string>();

	bool spawned = false;
	
	List<GameObject> runes = new List<GameObject>(); //Emitters are responsible for cleaning up their own runes.
	
	public void DeleteSpawner() {
		Cleanup ();
		Delete ();
	}
	
	public SerializableSpawner SerializeSpawner () {
		SerializableSpawner se = new SerializableSpawner ();
		se.pathscript = new List<int>();
		se.aspectscript = new List<string>();
		
		se.node = this.SerializeNode();
		se.desiredCount = desiredCount;

		se.emitsPatterns = emitsPatterns;
		se.patternToEmit = patternToEmit;
		foreach(int path in pathscript) {
			se.pathscript.Add (path);
		}
		foreach(string aspect in aspectscript) {
			se.aspectscript.Add (aspect);
		}
		
		return se;
		
	}
	public void DeserializeSpawner(SerializableSpawner se) {
		
		DeserializeNode(se.node);
		desiredCount = se.desiredCount;
		count = desiredCount;

		emitsPatterns = se.emitsPatterns;
		patternToEmit = se.patternToEmit;
		
		foreach(int path in se.pathscript) {
			pathscript.Add (path);
		}
		foreach(string aspect in se.aspectscript) {
			aspectscript.Add (aspect);
		}
		
	}
	void Start() {
		if (GlobalDefines.globalDefines == null) {
			Debug.Log ("Warning, GlobalDefines not found");
		}
	}
	
	public void UpdateTick (float time) {
		if (!spawned) {
			for (int x = 0; x < this.desiredCount; x++) {
				for (int p = 0; p < this.nextNode.Count; p++) {
					GameObject rune  = null;
					if (!emitsPatterns){
						rune = GameObject.Instantiate(GlobalDefines.globalDefines.SimpleRuneType);
					} else {
						rune = GameObject.Instantiate(GlobalDefines.globalDefines.SimplePatternType);
						rune.layer = 8;
					}
					rune.transform.position = gameObject.transform.position;
					rune.transform.parent = gameObject.transform;

					waypointNode nextNode = this.nextNode[p];
					simpleRuneMovement runeScript = rune.AddComponent<simpleRuneMovement>();
					runeScript.currentNode = this;
					runeScript.nextNode = nextNode;
				
					runeScript.moving = false;
					runeScript.direction = direction;
					runeScript.directionMethod = directionMethod;
					if (aspectscript.Count > aspectindex) {
						runeScript.setAspect(aspectscript[aspectindex]);
						
						aspectindex++;
						if (aspectindex >= aspectscript.Count) aspectindex = 0;
					}
					runes.Add(rune);
					if (emitsPatterns) {
						
						runePattern pat = rune.AddComponent<runePattern>();
						
						pat.DeserializePattern(patternToEmit);
						
						rune.GetComponent<runePattern>().BeginPattern(Time.time);
						rune.GetComponent<runePattern>().name = "Spawned";
					} 
					float amt = (float)(x+1) / (float)(this.desiredCount+1);
					runeScript.amt = amt;
					Vector3 nextPoint;
					if (nextNode.arc) {
						nextPoint = nextNode.getSlerp(gameObject.transform.position, amt);
					} else {
						nextPoint = Vector2.Lerp (gameObject.transform.position, nextNode.transform.position, amt);
					}
					rune.transform.position = nextPoint;


				}
			}
			spawned = true;
		}



	}
	
	public void Cleanup() {
		foreach (GameObject obj in runes) {
			if (obj != null) {
				Destroy(obj);
			}
		}
		runes.Clear ();

		count = desiredCount;
		aspectindex = 0;

		spawned = false;
	}
	
}


[Serializable]
public struct SerializableSpawner 
{
	public SerializableWaypointNode node;
	public int desiredCount;
	
	public SerializablePattern patternToEmit;
	public bool emitsPatterns;
	
	public List<int> pathscript;
	public List<string> aspectscript;
	
}
