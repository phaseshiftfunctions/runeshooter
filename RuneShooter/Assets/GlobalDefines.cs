﻿using UnityEngine;
using System.Collections;

public class GlobalDefines : MonoBehaviour {
	/* 
	 * Singleton class that hold all commonly needed types and other global variables. 
	 * 
	 */
	public static GlobalDefines globalDefines;


	public GameObject SimpleRuneType;		//Base Rune
	public GameObject SimpleNodeType;		//Base Node
	public GameObject SimplePatternType;	//Base Pattern

	public float RadToDeg = (180 / Mathf.PI);
	public Material LineRendererMaterial;			//Linerenderer material

	public float editorScale = 0.3f;		//The scale of the screen on the editor. Saves and loads from there apply this value.
	public bool scaleToEditor = true;		//If in editor, apply above scale
											//Scaling assumes the center of the screen is 0, 0
	public float aspectratio = 0.75f;		//Aspect ratio of the gamescreen. 

	public float timeRate = 1.0f;

	void Awake() {
		if (globalDefines == null) {
			DontDestroyOnLoad(gameObject);
			globalDefines = this;
		} else if (globalDefines != this) {
			Destroy(gameObject);
		}
	}

}
