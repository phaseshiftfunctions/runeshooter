﻿using UnityEngine;
using System;

[Serializable]
public struct SerializableVector3 {
	public float X;
	public float Y;
	public float Z;


	public SerializableVector3(float x, float y, float z)  //constructor
	{
		this.X = x;
		this.Y = y;
		this.Z = z;

	}
	
	static public implicit operator SerializableVector3(Vector3 value)
	{
		return new SerializableVector3(value.x, value.y, value.z);
	}
	static public implicit operator SerializableVector3(Vector2 value)
	{
		return new SerializableVector3(value.x, value.y, 0.0f);
	}
	static public implicit operator Vector3(SerializableVector3 value)
	{
		return new Vector3(value.X, value.Y, value.Z);
	}
}
