﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


public class LevelEditorScript : MonoBehaviour {
	public GameObject levelGO;
	SimpleLevel level;
	public Canvas canvas;
	public Slider TimeSlider;
	public InputField currentTimeField;
	public InputField durationTimeField;
	public GameObject patternsMenuPosition;
	public GameObject buttonPrefab;
	public GameObject patternHandle;

	string selectedButton = "";
	GameObject selectedPattern = null;

	Dictionary<string, GameObject> PatternButtons = new Dictionary<string, GameObject>();
	Dictionary<string, SerializablePattern> Patterns = new Dictionary<string, SerializablePattern>();
	Dictionary<int, GameObject> handles = new Dictionary<int, GameObject>();


	int selectedHandle = -1;

	bool active = true;

	float sliderY;
	float sliderStart;
	float sliderSize;
	void Start() {
		level = levelGO.GetComponent<SimpleLevel>();
		TimeSlider.maxValue = level.GetDuration();
		Vector3[] corners = new Vector3[4];
		TimeSlider.gameObject.GetComponent<RectTransform>().GetWorldCorners(corners);
		sliderStart = corners[0].x;
		sliderSize = corners[2].x - corners[0].x;
		sliderY = corners[1].y + 1f;
	}
	void Update() {
		if (EventSystem.current.IsPointerOverGameObject() || !active) {
			return;
		}
		if (level.IsStarted()) {
			TimeSlider.value = level.GetTime();
			currentTimeField.text = TimeSlider.value.ToString();
		}
		if (Input.GetMouseButtonDown(0)) {

			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit2D hitInfo =  Physics2D.Raycast(ray.origin, Vector2.zero);
			if (hitInfo)
			{
				GameObject clickedGO = hitInfo.collider.gameObject;
				if (clickedGO.name.StartsWith("HANDLE")) {
					int newIndex = int.Parse(clickedGO.GetComponent<dataValue>().data);

					SelectHandle(newIndex);
				}


			} else {


				if (Patterns.ContainsKey(selectedButton)) {
					SerializablePattern pattern = Patterns[selectedButton];
					Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					pos.z = 0;
					pattern.position = pos;
					int index = level.AddPattern(pattern, TimeSlider.value);
					GameObject newHandle = GameObject.Instantiate(patternHandle);
					newHandle.GetComponent<dataValue>().data = index.ToString();
					newHandle.transform.position = GetPositionOfHandle(index);

					newHandle.name = "HANDLE" + index.ToString();
					handles[index] = newHandle;
					SelectHandle(index);

				}
			}
		}
		if (Input.GetKeyDown(KeyCode.Delete) && selectedHandle != -1) {
			int deleteindex = selectedHandle;
			GameObject handle = handles[deleteindex];
			Destroy(handle);
			handles.Remove(deleteindex);
			selectedHandle = -1;
		} else if (Input.GetKeyDown(KeyCode.LeftArrow)) {
			SelectPreviousPattern();
		}else if (Input.GetKeyDown(KeyCode.RightArrow)) {
			SelectNextPattern();
		}
	}
	Vector3 GetPositionOfHandle(int index) {
		float layer = 0;
		SerializableLevelTimings set = level.GetPatternSet(index) ;
		float length = level.GetDuration();
		float rate = set.startTime/length;
		float x = sliderSize * rate;

		Vector3 pos = new Vector3(x + sliderStart, sliderY + 10f, 0f);
		Vector3 apos = new Vector3(x + sliderStart -0.1f, sliderY + 10f, 0f);
		Vector3 apos2 = new Vector3(x + sliderStart + 0.1f, sliderY + 1000f, 0f);

		Collider2D[] collider =  Physics2D.OverlapAreaAll(Camera.main.ScreenToWorldPoint(apos), Camera.main.ScreenToWorldPoint(apos2));
		pos.y = pos.y + (10f * collider.Length); //move the handle up for each overlapping handle.
		layer = layer + (1f * collider.Length); //move the handle up for each overlapping handle.	

		pos = Camera.main.ScreenToWorldPoint(pos);
		pos.z = layer;
		return pos;
	}



	void SelectHandle(int index) {
		if (handles.ContainsKey(selectedHandle)) {
			handles[selectedHandle].GetComponent<SpriteRenderer>().color = Color.white;
		}
		if (handles.ContainsKey(index)) {
			handles[index].GetComponent<SpriteRenderer>().color = Color.red;
		}
		selectedHandle = index;
		if (selectedPattern != null) {
			Destroy(selectedPattern);
		}
		selectedPattern = GameObject.Instantiate(GlobalDefines.globalDefines.SimplePatternType);

		runePattern pattern = selectedPattern.AddComponent<runePattern>();
		pattern.DeserializePattern(level.GetPatternSet(selectedHandle).pattern);



	}
	public void PlayLevelPressed() {
		level.SetTime(TimeSlider.value);
		level.StartLevel();

	}
	public void StopLevelPressed() {
		level.StopLevel();
		
	}
	public void HidePressed() {
		Camera.main.cullingMask ^= 1 << 8;
	}


	public void SwitchToPatternEditor() {
		canvas.enabled = (false);
		active = false;
	
	}
	
	public void SwitchToLevelEditor() {
		canvas.enabled =(true);
		active = true;

	}

	public void SetLevelTotalTime(string val) {
		float time;
		if (float.TryParse(val, out time)) {
			level.SetDuration(time);
		}

	}
	public void SetStartTime() {

		level.UpdatePatternTime(selectedHandle, level.GetTime());
		GameObject handle = handles[selectedHandle];
		handle.transform.position = GetPositionOfHandle(selectedHandle);

	}
	public void SetCurrentTime(string val) {
		float time;
		if (float.TryParse(val, out time)) {
			GoToTime(time);
		}
	}
	public void GoToTime(float val) {
		level.SetTime(val);
		TimeSlider.value = (val);
		currentTimeField.text = TimeSlider.value.ToString();
	}
	public void ClearPatterns() {
		foreach(GameObject button in PatternButtons.Values) {
			Destroy (button);
		}
		PatternButtons.Clear();
		Patterns.Clear();
	}
	public void LoadPatterns() {
		string filePath = Application.dataPath;
		string fileName = UnityEditor.EditorUtility.OpenFolderPanel("Load Pattern", filePath, "Patterns");
		string[] files = Directory.GetFiles(fileName);
		foreach (string file in files) {
			if (!file.EndsWith(".pat", System.StringComparison.InvariantCultureIgnoreCase )) {
				continue;
			}
			if (File.Exists(file)) {
				Debug.Log(file);
				SerializablePattern sp = new SerializablePattern();
				BinaryFormatter bformatter = new BinaryFormatter();
				FileStream stream = File.Open (file, FileMode.Open);
				
				sp = (SerializablePattern)bformatter.Deserialize(stream);
				CreatePatternButton (sp, file.Substring(file.LastIndexOf("\\")));
				stream.Close ();
			}
		}
	}

	public void PatternSelected(string val) {
		selectedButton = val;

	}

	void CreatePatternButton(SerializablePattern pattern, string name) {
		int maxRows = 20;


		GameObject button = GameObject.Instantiate(buttonPrefab);
		dataValue data = button.GetComponent<dataValue>();
		data.data = name;

		float height = button.GetComponent<RectTransform>().rect.height;
		float width = button.GetComponent<RectTransform>().rect.width;
		Vector3 pos = new Vector3(10f + (width/2),Screen.height - 50f, 0f);
		Vector3 offset = new Vector3 ((PatternButtons.Count/maxRows) * width, (PatternButtons.Count%maxRows) * height * -1);


		button.transform.position = pos + offset;
		button.transform.SetParent(canvas.transform, true);
		button.GetComponentInChildren<Text>().text = name;
		button.GetComponent<Button>().onClick.AddListener(() => PatternSelected(data.data));
		PatternButtons[name]= (button);
		Patterns[name] = pattern;
	}
	public void SelectNextPattern() {
		int current = level.GetIndexOfOrder(selectedHandle);
		int count = level.GetOrderCount();
		current++;
		if (current >= count){ 
			current = 0;
		}
		SelectHandle(level.GetOrder(current));
	}
	public void SelectPreviousPattern() {

		int current = level.GetIndexOfOrder(selectedHandle);
		int count = level.GetOrderCount();
		current--;
		if (current < 0){ 
			current = count-1;
		}
		SelectHandle(level.GetOrder(current));

	}

}
