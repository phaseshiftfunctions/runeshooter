using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class patternEditorScript : MonoBehaviour {
	//***************
	bool active = false;
	public GameObject linkMenu;
	public Canvas canvas;
	GameObject patternGO;
	runePattern pattern;

	int nodeType;
	int selectedInstance;
	float speed = 10;

	bool allowMove = false;
	waypointNode currentNode = null;
	waypointNode currentMovingNode = null;

	public GameObject  emitterCountText;
	public GameObject  emitterStartText;
	public GameObject  emitterIntervalText;
	public GameObject  emitterPathScriptText;
	public GameObject  emitterAspectScriptText;

	List<GameObject> pathTimeFields = new List<GameObject>();
	List<GameObject> pathSpeedFields = new List<GameObject>();

	public Button absButton;
	public Button relButton;
	public Button unchButton;
	public Button posButton;

	public Button emitterButton;
	public Button nodeButton;
	public Button patternButton;
	public Button spawnerButton;

	public Button manualPositionButton;
	public Button manualArcButton;
	public Button manualDirectionButton;

	enum ManualMode{
		POSITION = 0,
		DIRECTION = 1,
		ARC = 2
	}

	ManualMode manualMode = ManualMode.POSITION;


	public GameObject xField;
	public GameObject yField;


	simpleRuneMovement.DirectionMethod currentDirectionMethod = simpleRuneMovement.DirectionMethod.UNCHANGED;
	
	//***************
	// Use this for initialization
	void Start () {


		patternGO = GameObject.Instantiate(GlobalDefines.globalDefines.SimplePatternType);
		patternGO.layer = 8;
		Vector3 centerpos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/2, Screen.height/2, 0));
		centerpos.Set(centerpos.x, centerpos.y, 0f);
		patternGO.transform.position = centerpos; 


		pattern = patternGO.AddComponent<runePattern>();

		RenderScreenRepresentation();
		SelectEmitter();
	}
	
	// Update is called once per frame
	void Update () {
		if (!active) {
			return;
		}
	//	Debug.Log ( Input.mousePosition.ToString());
		if (EventSystem.current.IsPointerOverGameObject()) {
			return;
		}
		if (Input.GetMouseButtonDown(0)) {
			OnLeftClick();
		}
		if (Input.GetMouseButtonDown(1)) {
			OnRightClick();
		}

		if (Input.GetMouseButton(0))  {
			//while mouse down
			if (currentNode != currentMovingNode) {
				allowMove = false;
			}
			if (!Input.GetKey(KeyCode.LeftControl) && !Input.GetKey(KeyCode.LeftAlt) && !Input.GetKey(KeyCode.Tab)) {
				if (allowMove) {
					Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					pos.z = 0;
					currentMovingNode.transform.position = pos;
				}
			}
			if (currentMovingNode != null) {
				foreach (waypointNode node in currentMovingNode.getLastNodeList()) {
					float dist = Vector2.Distance(currentMovingNode.transform.position, node.transform.position);
					float time = dist/speed;
					node.setTravelTime(currentMovingNode.gameObject.GetInstanceID(),time, false);
				}
				foreach (waypointNode node in currentMovingNode.getNextNodeList()) {
					float dist = Vector2.Distance(currentMovingNode.transform.position, node.transform.position);
					float time = dist/speed;
					currentMovingNode.setTravelTime(node.gameObject.GetInstanceID(),time, false);
				}

				currentMovingNode.RenderLine();
				PopulateManualFields();
			}
		}


		if (Input.GetMouseButtonUp(0)) {
			if (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.LeftControl)) {
				selectNode(currentMovingNode);
				allowMove = true;
			} 
		}
		//Debug.Log (pattern.gameObject.transform.position.ToString() + "  " + (Camera.main.ScreenToWorldPoint(Input.mousePosition)-pattern.gameObject.transform.position).ToString());
	}

	void OnLeftClick() {
		//On mouse, place a new node, or select the clicked one.
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit2D hitInfo =  Physics2D.Raycast(ray.origin, Vector2.zero);
		if (hitInfo)
		{
			GameObject clickedGO = hitInfo.collider.gameObject;
			waypointNode clickedWN = clickedGO.GetComponent<waypointNode>();
			if (Input.GetKey(KeyCode.LeftControl)) {
				//add this node to the previous instead.
				if (currentNode != null && clickedWN.canLink && currentNode.canLink) {
					currentNode.addNextNode(clickedWN);
					clickedWN.addLastNode(currentNode);
					clickedWN.RenderLine();
				}
			
				
			} else {
	
				currentMovingNode = clickedWN;
			}
		
		} else {

			if (Input.GetKey(KeyCode.LeftAlt)) {
				Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				pos.z = 0;
				this.setArc(pos - pattern.transform.position);
			}else if (Input.GetKey(KeyCode.Tab)) {
				Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				pos.z = 0;
				if (currentDirectionMethod == simpleRuneMovement.DirectionMethod.ABSOLUTE) {
					setDirection( pos - currentNode.gameObject.transform.position, currentDirectionMethod);

				} else if (currentDirectionMethod == simpleRuneMovement.DirectionMethod.RELATIVE) {
					
					setDirection( pos - currentNode.gameObject.transform.position, currentDirectionMethod);
					
				} else if (currentDirectionMethod == simpleRuneMovement.DirectionMethod.POSITION) { 
					
					setDirection(pos - pattern.transform.position, currentDirectionMethod);

				}
			} else {
				GameObject node = GameObject.Instantiate(GlobalDefines.globalDefines.SimpleNodeType);
				node.transform.parent = patternGO.transform;
				node.layer = 8;
				Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				pos.z = 0;
				node.transform.position = pos;
				if (nodeType == 1) {

					currentMovingNode = node.AddComponent<simpleRuneEmitter>();

					pattern.AddEmitter((simpleRuneEmitter)currentMovingNode);
				} else if (nodeType == 2) {
					currentMovingNode = node.AddComponent<waypointNode>();

					pattern.AddNode(currentMovingNode);
					if (currentNode != null) {
						if (currentNode.canLink && currentMovingNode.canLink) {
							currentMovingNode.addLastNode(currentNode);
							currentNode.addNextNode(currentMovingNode);
						}
					}
				} else if (nodeType == 3) {
					SerializablePattern pat = LoadPattern();
					currentMovingNode = node.AddComponent<simpleRuneEmitter>();

					((simpleRuneEmitter)currentMovingNode).emitsPatterns = true;
					((simpleRuneEmitter)currentMovingNode).patternToEmit = pat;
					pattern.AddEmitter((simpleRuneEmitter)currentMovingNode);


				} else if (nodeType == 4) {
					currentMovingNode = node.AddComponent<spawnerNode>();
					
					pattern.AddSpawner((spawnerNode)currentMovingNode);
					
					
				} 


			}

		}
	}

	void OnRightClick() {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit2D hitInfo =  Physics2D.Raycast(ray.origin, Vector2.zero);
		if (hitInfo)
		{

			GameObject node = hitInfo.collider.gameObject;

			waypointNode wn =  node.GetComponent<waypointNode>();
			if (Input.GetKey(KeyCode.LeftControl)) {
				currentNode.removeNextNode(wn.GetInstanceID());
				wn.removeLastNode(currentNode.GetInstanceID());
				wn.removeNextNode(currentNode.GetInstanceID());
				currentNode.removeLastNode(wn.GetInstanceID());

				currentNode.RenderLine();
			} else {
				pattern.RemoveNode(wn);
				wn.Delete();

			}
		} else {
			if (Input.GetKey(KeyCode.LeftAlt)) {
				currentNode.arc = false;
				currentNode.RenderLine();
			}
		}
	}

	void setDirection(Vector3 pos, simpleRuneMovement.DirectionMethod method) {
		currentNode.directionMethod = method;
		if (currentNode.directionMethod == simpleRuneMovement.DirectionMethod.ABSOLUTE) {
			
			float deltaX =  pos.x;
			float deltaY =  pos.y;
			float rad = Mathf.Atan2(deltaX, deltaY); // In radians
			float deg = rad * GlobalDefines.globalDefines.RadToDeg;
			Vector3 direction = new Vector3(pos.x, pos.y, -1*deg); //Use z to hold the degrees, x and y to hold the position for the editor.

			currentNode.direction = direction;
			
		} else if (currentNode.directionMethod == simpleRuneMovement.DirectionMethod.RELATIVE) {
			
			float deltaX =  pos.x;
			float deltaY =  pos.y;
			float rad = Mathf.Atan2(deltaX, deltaY); // In radians
			float deg = rad * GlobalDefines.globalDefines.RadToDeg;
			Vector3 direction = new Vector3(pos.x, pos.y, -1*deg);//Use z to hold the degrees, x and y to hold the position for the editor.

			currentNode.direction = direction;
			
		} else if (currentNode.directionMethod == simpleRuneMovement.DirectionMethod.POSITION) { 
			
			
			currentNode.direction = pos;
		}

		
	}

	void setArc(Vector3 pos) {
		if (currentNode.canLink) {
			currentNode.arc = true;
			currentNode.arcCenter = pos;

			currentNode.RenderLine();
			
		}
	}
	
	void selectNode(waypointNode node) {
		if (currentNode != null) {
			if (currentNode.locked) {
				currentNode.GetComponent<SpriteRenderer>().color = Color.green;
			} else {
				currentNode.GetComponent<SpriteRenderer>().color = Color.white;
			}
		}
		if (node.locked) {
			node.GetComponent<SpriteRenderer>().color = Color.black;
		} else {
			node.GetComponent<SpriteRenderer>().color = Color.red;
		}

		currentNode = node;
		if (currentNode.GetType() == typeof(simpleRuneEmitter)) {
			InputField text = emitterCountText.GetComponent<InputField>(); 
			text.text = ((simpleRuneEmitter)currentNode).desiredCount.ToString();
			text = emitterStartText.GetComponent<InputField>(); 
			text.text = ((simpleRuneEmitter)currentNode).startTime.ToString();
			text = emitterIntervalText.GetComponent<InputField>(); 
			text.text = ((simpleRuneEmitter)currentNode).interval.ToString();
			text = emitterPathScriptText.GetComponent<InputField>(); 
			text.text = "";
			foreach (int val in ((simpleRuneEmitter)currentNode).pathscript) {
				text.text += val.ToString() + ",";

			}
			text = emitterAspectScriptText.GetComponent<InputField>(); 
			text.text = "";
			foreach (string val in ((simpleRuneEmitter)currentNode).aspectscript) {
				text.text += val + ",";
				
			}
		} else if (currentNode.GetType() == typeof(spawnerNode)) {
			InputField text = emitterCountText.GetComponent<InputField>(); 
			text.text = ((spawnerNode)currentNode).desiredCount.ToString();
			text = emitterStartText.GetComponent<InputField>(); 
			text.text = "";
			text = emitterIntervalText.GetComponent<InputField>(); 
			text.text = "";
			text = emitterPathScriptText.GetComponent<InputField>(); 
			text.text = "";
			foreach (int val in ((spawnerNode)currentNode).pathscript) {
				text.text += val.ToString() + ",";
				
			}
			text = emitterAspectScriptText.GetComponent<InputField>(); 
			text.text = "";
			foreach (string val in ((spawnerNode)currentNode).aspectscript) {
				text.text += val + ",";
				
			}
		} else {
			InputField text = emitterCountText.GetComponent<InputField>(); 
			text.text = "";
			text = emitterStartText.GetComponent<InputField>(); 
			text.text = "";
			text = emitterIntervalText.GetComponent<InputField>(); 
			text.text = "";
			text = emitterPathScriptText.GetComponent<InputField>(); 
			text.text = "";
			text = emitterAspectScriptText.GetComponent<InputField>(); 
			text.text = "";
		}




		CreatePathInputFields();
	}

	public void TogglePause() {
		if(currentNode.GetStaticBehaviour() == null){

			currentNode.ApplyStaticBehaviour(currentNode.gameObject.AddComponent<pauseBehaviour>());
		//	ChangeInterval(PauseTextField.text);
		} else {
			currentNode.RemoveStaticBehaviour();
		}
		

	}
	public void ChangeInterval(string val) {

		float interval;
		if (float.TryParse(val, out interval)) {
			baseNodeBehaviour beh = currentNode.GetStaticBehaviour();
			beh.duration = interval;
		}
	}
	public void NewPattern () {
		pattern.Reset();
		nodeType = 1;

		currentNode = null;
		currentMovingNode = null;
		foreach( GameObject field in pathTimeFields) {
			Destroy(field);
		}
		foreach( GameObject field in pathSpeedFields) {
			Destroy(field);
		}
	}
	public void PlayPattern () {
		pattern.BeginPattern(Time.time);
	}
	public void StopPattern () {
		pattern.EndPattern();
	}
	public void ChangeEmitterCount(string val) {
		int count;
		if (int.TryParse(val, out count)) {
			Debug.Log (currentNode.GetType());
			if (currentNode.GetType() == typeof(simpleRuneEmitter)) {
				((simpleRuneEmitter)currentNode).desiredCount = count;
				((simpleRuneEmitter)currentNode).count = count;
			} else 	if (currentNode.GetType() == typeof(spawnerNode)) {
				((spawnerNode)currentNode).desiredCount = count;
				((spawnerNode)currentNode).count = count;
			}
		}
	}
	public void ChangeEmitterStart(string val) {
		float start;
		if (float.TryParse(val, out start)) {

			if (currentNode.GetType() == typeof(simpleRuneEmitter)) {
				((simpleRuneEmitter)currentNode).startTime = start;
			}
		}
	}
	public void ChangeEmitterInterval(string val) {
		float interval;
		if (float.TryParse(val, out interval)) {

			if (currentNode.GetType() == typeof(simpleRuneEmitter)) {
				((simpleRuneEmitter)currentNode).interval = interval;
			}
		}
	}
	public void SetSelectedTravelTimeInstance(string val) {

		int inst;
		if (int.TryParse(val, out inst)) {

			selectedInstance = inst;

		}
	}
	public void ChangeEmitterPathScript(string val) {

		if (currentNode.GetType() == typeof(simpleRuneEmitter)) {

			string[] words = val.Split(',');

			List<int> script = new List<int>();
			foreach (string s in words)
			{
				s.Trim();
				int path;
				
				if (!int.TryParse(s, out path)) {
					continue;
				}
				script.Add (path);
			}			

			((simpleRuneEmitter)currentNode).pathscript = script;

		}

	}
	public void ChangeEmitterAspectScript(string val) {
		
		if (currentNode.GetType() == typeof(simpleRuneEmitter)) {
			
			string[] words = val.Split(',');
			
			List<string> script = new List<string>();
			foreach (string s in words)
			{
				s.Trim();

				script.Add (s);
			}			
			
			((simpleRuneEmitter)currentNode).aspectscript = script;
			
		}
		
	}
	public void ChangeTravelTime(string val) {
		float interval;

		if (float.TryParse(val, out interval)) {
			List<waypointNode> lastNodes = currentNode.getLastNodeList();
			foreach(waypointNode node in lastNodes) {
				if (selectedInstance == node.GetInstanceID()) {
					node.setTravelTime(currentNode.gameObject.GetInstanceID(), interval, true);
				}
			}
			lastNodes = currentNode.getNextNodeList();
			foreach(waypointNode node in lastNodes) {
				if (selectedInstance == node.GetInstanceID()) {
					currentNode.setTravelTime(node.gameObject.GetInstanceID(), interval, true);
				}
			}



		}
	}
	public void ChangeTravelSpeed(string val) {
		float speed;
		
		if (float.TryParse(val, out speed)) {
			List<waypointNode> lastNodes = currentNode.getLastNodeList();
			foreach(waypointNode node in lastNodes) {
				if (selectedInstance == node.GetInstanceID()) {

					float dist = Vector2.Distance(currentMovingNode.transform.position, node.transform.position);
					float time = dist/speed;

					node.setTravelTime(currentNode.gameObject.GetInstanceID(), time, true);
				}
			}
			lastNodes = currentNode.getNextNodeList();
			foreach(waypointNode node in lastNodes) {
				if (selectedInstance == node.GetInstanceID()) {

					float dist = Vector2.Distance(currentMovingNode.transform.position, node.transform.position);
					float time = dist/speed;
					currentNode.setTravelTime(node.gameObject.GetInstanceID(),time, true);

				}
			}
			
			
			
		}
	}



	void CreatePathInputFields() {
		foreach( GameObject field in pathTimeFields) {
			Destroy(field);
		}
		foreach( GameObject field in pathSpeedFields) {
			Destroy(field);
		}
		Vector2 speedOffset = new Vector2(0f, -20);
		List<waypointNode> lastNodes = currentNode.getLastNodeList();
		foreach(waypointNode node in lastNodes) {
			Vector2 lpos =  Vector2.Lerp (currentNode.gameObject.transform.position, node.gameObject.transform.position, 0.5f);
			if (currentNode.arc) {
				lpos = node.getSlerp(currentNode.gameObject.transform.position, 0.5f);
			} 
			Vector2 fieldspos = Camera.main.WorldToScreenPoint(lpos);

			//Time Field
			GameObject newTimeField = GameObject.Instantiate(linkMenu);
			dataValue data = newTimeField.AddComponent<dataValue>();
			data.data = node.GetInstanceID().ToString();
			newTimeField.transform.SetParent(canvas.transform, false);
			newTimeField.transform.position = fieldspos;
			InputField IF = newTimeField.GetComponent<InputField>();
			IF.text = node.getTravelTime(currentNode.gameObject.GetInstanceID()).ToString();
			IF.onEndEdit.AddListener((value) => SetSelectedTravelTimeInstance(data.data));
			IF.onEndEdit.AddListener((value) =>  ChangeTravelTime(value));
			pathTimeFields.Add (newTimeField);
			//Speed Field
			GameObject newSpeedField = GameObject.Instantiate(linkMenu);
			dataValue data2 = newSpeedField.AddComponent<dataValue>();
			data2.data = node.GetInstanceID().ToString();
			newSpeedField.transform.SetParent(canvas.transform, false);
			newSpeedField.transform.position = fieldspos + speedOffset;
			InputField IF2 = newSpeedField.GetComponent<InputField>();

			float time = node.getTravelTime(currentNode.gameObject.GetInstanceID());
			float dist = Vector2.Distance(currentNode.transform.position, node.transform.position);


			IF2.text = (dist/time).ToString ();

			IF2.onEndEdit.AddListener((value) => SetSelectedTravelTimeInstance(data.data));
			IF2.onEndEdit.AddListener((value) =>  ChangeTravelSpeed(value));
			pathSpeedFields.Add (newSpeedField);

		}

		List<waypointNode> nextNodes = currentNode.getNextNodeList();

		foreach(waypointNode node in nextNodes) {
			Vector2 lpos =  Vector2.Lerp (currentNode.gameObject.transform.position, node.gameObject.transform.position, 0.5f);
			if (node.arc) {
				lpos = currentNode.getSlerp(node.gameObject.transform.position, 0.5f);
			} 
			Vector2 fieldspos = Camera.main.WorldToScreenPoint(lpos);


			GameObject newTimeField = GameObject.Instantiate(linkMenu);
			dataValue data = newTimeField.AddComponent<dataValue>();
			data.data = node.GetInstanceID().ToString();
			InputField IF = newTimeField.GetComponent<InputField>();

			IF.text = currentNode.getTravelTime(node.gameObject.GetInstanceID()).ToString();
			IF.onEndEdit.AddListener((value) => SetSelectedTravelTimeInstance(data.data));
			IF.onEndEdit.AddListener((value) =>  ChangeTravelTime(value));

			newTimeField.transform.SetParent(canvas.transform);
			newTimeField.transform.position = fieldspos;
			pathTimeFields.Add (newTimeField);

			GameObject newSpeedField = GameObject.Instantiate(linkMenu);
			dataValue data2 = newSpeedField.AddComponent<dataValue>();
			data2.data = node.GetInstanceID().ToString();
			InputField IF2 = newSpeedField.GetComponent<InputField>();
			
			float time = currentNode.getTravelTime(node.gameObject.GetInstanceID());
			float dist = Vector2.Distance(currentNode.transform.position, node.transform.position);
			
			
			IF2.text = (dist/time).ToString ();

			IF2.onEndEdit.AddListener((value) => SetSelectedTravelTimeInstance(data.data));
			IF2.onEndEdit.AddListener((value) =>  ChangeTravelSpeed(value));
			
			newSpeedField.transform.SetParent(canvas.transform);
			newSpeedField.transform.position = fieldspos  + speedOffset;
			pathSpeedFields.Add (newSpeedField);

			
		}
	}
	
	public void ToggleShowNav() {
		Camera.main.cullingMask ^= 1 << 8;
	}
	public void PopulateManualFields() {
		if (currentNode != null) {
			if (manualMode == ManualMode.ARC) {

				xField.GetComponent<InputField>().text = currentNode.arcCenter.x.ToString();
				yField.GetComponent<InputField>().text = currentNode.arcCenter.y.ToString();
			} else if (manualMode == ManualMode.DIRECTION) {
				xField.GetComponent<InputField>().text = currentNode.direction.x.ToString();
				yField.GetComponent<InputField>().text = currentNode.direction.y.ToString();
			} else if (manualMode == ManualMode.POSITION) {
				if (currentNode.gameObject.transform.parent != null) {
					Vector3 pos = currentNode.gameObject.transform.position;
					Vector3 relPos = pos - currentNode.gameObject.transform.parent.position;
					xField.GetComponent<InputField>().text = relPos.x.ToString();
					yField.GetComponent<InputField>().text = relPos.y.ToString();
				}
			} else {
				xField.GetComponent<InputField>().text = "";
				yField.GetComponent<InputField>().text = "";
			}
		}
	}
	public void ManuallyEnterPosition() {
		int x;
		if (!int.TryParse(xField.GetComponent<InputField>().text, out x)) {
			
			return;
			
		}
		int y;
		if (!int.TryParse(yField.GetComponent<InputField>().text, out y)) {
			
			return;
			
		}
		Vector3 newPos = new Vector3(x, y, 0f);
		if (manualMode == ManualMode.ARC) {
			setArc (newPos);
		} else if (manualMode == ManualMode.DIRECTION) {

			setDirection(newPos, currentDirectionMethod);
		} else if (manualMode == ManualMode.POSITION){

			currentMovingNode.transform.position = newPos + pattern.gameObject.transform.position;
		}


		

	}

	void RenderScreenRepresentation() {

		int height = (int)(Screen.height * GlobalDefines.globalDefines.editorScale);
		int width = (int)(height * GlobalDefines.globalDefines.aspectratio);
		Debug.Log (height + "  " + width + "  " +  GlobalDefines.globalDefines.editorScale);
       	int centerwidth = (int)(Screen.width * 0.5);
		int centerheight = (int)(Screen.height * 0.5);
		float x = width/2 ;
		float y = height/2 ;

		Vector3 postl = Camera.main.ScreenToWorldPoint(new Vector3(centerwidth -x, centerheight+ y, 0));


		Vector3 postr = Camera.main.ScreenToWorldPoint(new Vector3(centerwidth + x, centerheight + y, 0));


		Vector3 posbl = Camera.main.ScreenToWorldPoint(new Vector3(centerwidth - x, centerheight - y, 0));


		Vector3 posbr = Camera.main.ScreenToWorldPoint(new Vector3(centerwidth + x, centerheight - y, 0));

		postl.Set(postl.x, postl.y, 0);
		postr.Set(postr.x, postr.y, 0);
		posbr.Set(posbr.x, posbr.y, 0);
		posbl.Set(posbl.x, posbl.y, 0);

		LineRenderer tline = this.CreateLineRenderer(postl);

		tline.material = GlobalDefines.globalDefines.LineRendererMaterial;
		tline.SetVertexCount(2);
		tline.SetPosition(0, postl);
		tline.SetPosition(1, postr);


		LineRenderer rline = this.CreateLineRenderer(postr);

		rline.material = GlobalDefines.globalDefines.LineRendererMaterial;
		rline.SetVertexCount(2);
		rline.SetPosition(0, postr);
		rline.SetPosition(1, posbr);


		LineRenderer bline = this.CreateLineRenderer(posbr);

		bline.material = GlobalDefines.globalDefines.LineRendererMaterial;
		bline.SetVertexCount(2);
		bline.SetPosition(0, posbr);
		bline.SetPosition(1, posbl);


		LineRenderer lline = this.CreateLineRenderer(posbl);

		lline.material = GlobalDefines.globalDefines.LineRendererMaterial;
		lline.SetVertexCount(2);
		lline.SetPosition(0, posbl);
		lline.SetPosition(1, postl);
	}

	
	LineRenderer CreateLineRenderer(Vector3 pos) {
		GameObject newObject= new GameObject();

		newObject.transform.position = pos;
		LineRenderer newLine = newObject.AddComponent<LineRenderer>();
		newLine.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		newLine.receiveShadows = false;
		newLine.useLightProbes = false;
		newLine.SetWidth(0.2f, 0.2f);
		newLine.SetColors(Color.green, Color.green);
		newLine.enabled = true;
		return newLine;
	}

	public void SavePattern() {

		string filePath = Application.dataPath;

		string fileName = UnityEditor.EditorUtility.SaveFilePanel("Save Pattern", filePath, "newPattern", "pat");

		SerializablePattern sp = pattern.SerializePattern();

		BinaryFormatter bformatter = new BinaryFormatter();
		FileStream stream = File.Open(fileName, FileMode.Create);

		bformatter.Serialize(stream, sp);

		stream.Close();
	}
	
	public void LoadPatternPressed() {

		SerializablePattern loadedPattern = LoadPattern();

		pattern.Reset();
		Destroy(patternGO);
		
		patternGO = GameObject.Instantiate(GlobalDefines.globalDefines.SimplePatternType);
		patternGO.layer = 8;
		pattern = patternGO.AddComponent<runePattern>();

		pattern.DeserializePattern(loadedPattern);



	}
	
	public SerializablePattern LoadPattern() {
		string filePath = Application.dataPath;
		
		string fileName = UnityEditor.EditorUtility.OpenFilePanel("Load Pattern", filePath, "pat");
		SerializablePattern sp = new SerializablePattern();
		if (File.Exists(fileName)) {
			BinaryFormatter bformatter = new BinaryFormatter();
			FileStream stream = File.Open (fileName, FileMode.Open);

			sp = (SerializablePattern)bformatter.Deserialize(stream);
			stream.Close ();

		} 
		return sp;
				
	}
	public void SetAbsoluteDirection() {
		currentDirectionMethod = simpleRuneMovement.DirectionMethod.ABSOLUTE;
		absButton.image.color = Color.gray;
		relButton.image.color = Color.white;
		posButton.image.color = Color.white;
		unchButton.image.color = Color.white;
	}
	public void SetRelativeDirection() {
		currentDirectionMethod = simpleRuneMovement.DirectionMethod.RELATIVE;
		absButton.image.color = Color.white;
		relButton.image.color = Color.gray;
		posButton.image.color = Color.white;
		unchButton.image.color = Color.white;
	}
	public void SetPositionDirection() {
		currentDirectionMethod = simpleRuneMovement.DirectionMethod.POSITION;
		absButton.image.color = Color.white;
		relButton.image.color = Color.white;
		posButton.image.color = Color.gray;
		unchButton.image.color = Color.white;

	}
	public void SetUnchangedDirection() {
		currentDirectionMethod = simpleRuneMovement.DirectionMethod.UNCHANGED;
		absButton.image.color = Color.white;
		relButton.image.color = Color.white;
		posButton.image.color = Color.white;
		unchButton.image.color = Color.gray;
	}

	public void SelectEmitter() {
		nodeType = 1;
		emitterButton.image.color = Color.gray;
		nodeButton.image.color = Color.white;
		patternButton.image.color = Color.white;
		spawnerButton.image.color = Color.white;
	}
	public void SelectNode() {
		nodeType = 2;
		emitterButton.image.color = Color.white;
		nodeButton.image.color = Color.gray;
		patternButton.image.color = Color.white;
		spawnerButton.image.color = Color.white;
	}
	public void SelectPatternEmitter() {
		nodeType = 3;
		emitterButton.image.color = Color.white;
		nodeButton.image.color = Color.white;
		patternButton.image.color = Color.gray;
		spawnerButton.image.color = Color.white;
	}
	public void SelectSpawner() {
		nodeType = 4;
		emitterButton.image.color = Color.white;
		nodeButton.image.color = Color.white;
		patternButton.image.color = Color.white;
		spawnerButton.image.color = Color.gray;
	}

	public void SelectManualPosition() {
		manualMode = ManualMode.POSITION;
		PopulateManualFields();
		manualPositionButton.image.color = Color.gray;
		manualArcButton.image.color = Color.white;
		manualDirectionButton.image.color = Color.white;
	}
	public void SelectManualArc() {
		manualMode = ManualMode.ARC;
		PopulateManualFields();
		manualPositionButton.image.color = Color.white;
		manualArcButton.image.color = Color.gray;
		manualDirectionButton.image.color = Color.white;
	}
	public void SelectManualDirection() {
		manualMode = ManualMode.DIRECTION;
		PopulateManualFields();
		manualPositionButton.image.color = Color.white;
		manualArcButton.image.color = Color.white;
		manualDirectionButton.image.color = Color.gray;
	}

	
	public void SwitchToPatternEditor() {
		canvas.enabled =(true);
		pattern.enabled = true;
		active = true;
	}
	
	public void SwitchToLevelEditor() {
		canvas.enabled =(false);
		StopPattern();
		pattern.enabled = false;
		active = false;
	}
}
