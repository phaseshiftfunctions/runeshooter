﻿using UnityEngine;
using System.Collections;

public class spinBehaviour : baseNodeBehaviour {

	void Start () {
		duration = 1f;
	}
	
	public override bool performBehaviours(float amt, simpleRuneMovement rune) {
		float interval = amt / duration;
		if (interval >= 1) {
			interval = 1.0f;
		}

		Quaternion target = Quaternion.Euler(0, 0, 360 * interval);
		rune.gameObject.transform.rotation = target;
		if (interval >= 1) {
			return true;
		}

		return false;
	}
	
	
}