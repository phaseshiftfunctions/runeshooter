﻿using UnityEngine;
using System.Collections;

public abstract class baseNodeBehaviour : MonoBehaviour {
	/*
	 * on arrival at a node, a rune will call the nodes behaviour.
	 * it will continue to do so, every frame, until the behaviour indicates it is completed. 
	 * 
	 * 
	 */
	public float duration = 0;
	public abstract bool performBehaviours(float amt, simpleRuneMovement rune);

}
