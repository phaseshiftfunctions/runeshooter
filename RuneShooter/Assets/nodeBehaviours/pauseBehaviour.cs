﻿using UnityEngine;
using System.Collections;

public class pauseBehaviour : baseNodeBehaviour {

	void Start () {
		duration = 0.5f;
	}

	public override bool performBehaviours(float amt, simpleRuneMovement rune) {
		float interval = amt / duration;
		if (interval >= 1) {
			return true;
		}


		return false;
	}


}