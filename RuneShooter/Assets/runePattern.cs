﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class runePattern : MonoBehaviour {
	bool started = false;
	float startedAt = 0;

	public string patternName = "";
	List<simpleRuneEmitter> emitters = new List<simpleRuneEmitter>();
	List<waypointNode> nodes = new List<waypointNode>();
	List<spawnerNode> spawners = new List<spawnerNode>();
	~runePattern() {
		Reset();
	}

	public SerializablePattern SerializePattern() {
		SerializablePattern sp = new SerializablePattern();

		sp.nodes = new List<SerializableWaypointNode>();
		sp.emitters = new List<SerializableEmitter>();
		sp.spawners = new List<SerializableSpawner>();
		if (GlobalDefines.globalDefines.scaleToEditor) {
			sp.position = gameObject.transform.position / GlobalDefines.globalDefines.editorScale;
		} else {
			sp.position = gameObject.transform.position;
		}

		foreach(simpleRuneEmitter emitter in emitters) {
			sp.emitters.Add (emitter.SerializeEmitter());
		}
		foreach(waypointNode node in nodes) {
			sp.nodes.Add (node.SerializeNode());
		}
		foreach(spawnerNode spawner in spawners) {
			sp.spawners.Add (spawner.SerializeSpawner());
		}
		return sp;
	}
	public void DeserializePattern(SerializablePattern ptrn) {
		Reset();
		gameObject.transform.position = ptrn.position;
		if (GlobalDefines.globalDefines.scaleToEditor) {
			gameObject.transform.position *= GlobalDefines.globalDefines.editorScale;
		} 

		Dictionary<int, waypointNode> nodeIDs = new Dictionary<int, waypointNode>();
		if (ptrn.emitters != null) {
			foreach(SerializableEmitter seremitter in ptrn.emitters) {
				GameObject node = GameObject.Instantiate(GlobalDefines.globalDefines.SimpleNodeType);
				node.transform.parent = this.gameObject.transform;
				node.layer = 8;
				simpleRuneEmitter emitter = node.AddComponent<simpleRuneEmitter>();

				emitter.DeserializeEmitter(seremitter);

				AddEmitter((simpleRuneEmitter)emitter);
				nodeIDs[seremitter.node.nodeID] = emitter;
				emitters.Add (emitter);

			}
		}
		if (ptrn.nodes != null) {
			foreach(SerializableWaypointNode sernode in ptrn.nodes) {
				GameObject nodego = GameObject.Instantiate(GlobalDefines.globalDefines.SimpleNodeType);
				nodego.layer = 8;
				nodego.transform.parent = this.gameObject.transform;
				waypointNode node = nodego.AddComponent<waypointNode>();
				node.DeserializeNode(sernode);

				nodeIDs[sernode.nodeID] = node;
				nodes.Add (node);
			}
		}
		if (ptrn.spawners != null) {
			foreach(SerializableSpawner serspawner in ptrn.spawners) {
				GameObject node = GameObject.Instantiate(GlobalDefines.globalDefines.SimpleNodeType);
				node.transform.parent = this.gameObject.transform;
				node.layer = 8;
				spawnerNode spawner = node.AddComponent<spawnerNode>();
				
				spawner.DeserializeSpawner(serspawner);
				
				AddSpawner((spawnerNode)spawner);
				nodeIDs[serspawner.node.nodeID] = spawner;
				spawners.Add (spawner);
			}
		}
		//Need to loop through and assing route pointers.
		if (ptrn.emitters != null) {
			foreach(SerializableEmitter seremitter in ptrn.emitters) {
				int id = seremitter.node.nodeID;
				foreach (int nodeid in seremitter.node.lastNode) {
					nodeIDs[id].addLastNode(nodeIDs[nodeid]);
				}
				foreach (int nodeid in seremitter.node.nextNode) {
					nodeIDs[id].addNextNode(nodeIDs[nodeid]);
				}
			}
		}
		if (ptrn.spawners != null) {
			foreach(SerializableSpawner serspawner in ptrn.spawners) {
				int id = serspawner.node.nodeID;
				foreach (int nodeid in serspawner.node.lastNode) {
					nodeIDs[id].addLastNode(nodeIDs[nodeid]);
				}
				foreach (int nodeid in serspawner.node.nextNode) {
					nodeIDs[id].addNextNode(nodeIDs[nodeid]);
				}
			}
		}
		if (ptrn.nodes != null) {
			foreach(SerializableWaypointNode sernode in ptrn.nodes) {
				int id = sernode.nodeID;
				foreach (int nodeid in sernode.lastNode) {
					nodeIDs[id].addLastNode(nodeIDs[nodeid]);
				}
				for (int x = 0; x < sernode.nextNode.Count; x++) {
					int nodeid = sernode.nextNode[x];
					float time = sernode.nextNodeTimes[x];
					nodeIDs[id].addNextNode(nodeIDs[nodeid]);
					nodeIDs[id].setTravelTime(nodeIDs[nodeid].gameObject.GetInstanceID(), time, false);
					Debug.Log(nodeid + " " + time.ToString());
				}

			}
		}
		
		foreach( waypointNode node in nodes) {
			node.RenderLine();
		}
		foreach( simpleRuneEmitter node in emitters) {
			node.RenderLine();
		}
		foreach( spawnerNode node in spawners) {
			node.RenderLine();
		}
		
	}
	public void Reset() {
		foreach(simpleRuneEmitter emitter in emitters) {
			emitter.DeleteEmitter();
		}		
		foreach(spawnerNode spawner in spawners) {
			spawner.DeleteSpawner();
		}
		foreach(waypointNode node in nodes) {
			node.Delete();
		}

		spawners.Clear();
		emitters.Clear();
		nodes.Clear();
	}

	// Update is called once per frame
	void Update () {
		float time = Time.time - startedAt;
		if (started) {


			foreach ( simpleRuneEmitter emitter in emitters) {
				emitter.UpdateTick(time);
			}


			
		}
	}

	public void BeginPattern(float time) {
		startedAt = time;
		started = true;
		foreach ( spawnerNode spawner in spawners) {
			spawner.UpdateTick(0f);
		}
	}
	public void EndPattern() {
		startedAt = 0;
		started = false;
		foreach ( simpleRuneEmitter emitter in emitters) {
			emitter.Cleanup();
		}
		foreach ( spawnerNode spawner in spawners) {
			spawner.Cleanup();
		}
	}

	public void AddNode(waypointNode node) {
		nodes.Add(node);
	}
	public void AddEmitter(simpleRuneEmitter node) {
		emitters.Add(node);
	}
	public void AddSpawner(spawnerNode node) {
		spawners.Add(node);
	}
	public void RemoveNode(waypointNode nodetoremove) {

		nodes.RemoveAll(node => node.gameObject.GetInstanceID() == nodetoremove.gameObject.GetInstanceID());
		emitters.RemoveAll(node => node.gameObject.GetInstanceID() == nodetoremove.gameObject.GetInstanceID());
		spawners.RemoveAll(node => node.gameObject.GetInstanceID() == nodetoremove.gameObject.GetInstanceID());
	}
}

[Serializable]
public struct SerializablePattern
{
	public SerializableVector3 position;
	public List<SerializableEmitter> emitters;
	public List<SerializableWaypointNode> nodes;
	public List<SerializableSpawner> spawners;
}
