﻿using UnityEngine;
using System.Collections;

public class BackgroundScroller : MonoBehaviour {
	public float scrollSpeed = 50f;
	public float tileSize = 2048f;
	public float level = 100;
	Vector3 startPos;
	// Use this for initialization
	void Start () {
		startPos = gameObject.transform.position;
		startPos.z = level;
	}
	
	// Update is called once per frame
	void Update () {
		float newPosition = Mathf.Repeat(Time.time * (scrollSpeed/level) * GlobalDefines.globalDefines.timeRate, tileSize);
		gameObject.transform.position = startPos + (Vector3.down*newPosition);
	}
}
