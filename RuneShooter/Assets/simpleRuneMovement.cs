﻿using UnityEngine;
using System.Collections;

public class simpleRuneMovement : MonoBehaviour {


	public waypointNode currentNode = null;
	public waypointNode nextNode = null;
	public int path = 0;
	public string aspect = "";

	public Vector3 direction = new Vector3(0f, 0f, 0f);
	public DirectionMethod directionMethod =  DirectionMethod.RELATIVE;
	public enum DirectionMethod {
		RELATIVE = 0,
		ABSOLUTE = 1,
		POSITION = 2,
		UNCHANGED = 3
	}
	public float amt = 0f; //The percent of distance along the line;
	
	float travelTime;

	public bool moving = true;



	bool behaviourExecuted = false;
	// Use this for initialization
	void Start () {
		if (currentNode != null) {
			gameObject.transform.position = currentNode.gameObject.transform.position;

			travelTime = 0;

			direction = currentNode.direction;
			directionMethod = currentNode.directionMethod;

		}
	}
	
	// Update is called once per frame
	void Update () {
		if (currentNode == null || nextNode == null) {
			Destroy (this);
			return;
		}

		float LastDeltaTime = Time.deltaTime * GlobalDefines.globalDefines.timeRate;;
		travelTime += LastDeltaTime;
		if (behaviourExecuted) {
		//	float distance = Vector2.Distance(gameObject.transform.position, nextNode.transform.position);
			nextNode.PerformMovingBehaviour(travelTime, this);
			if (moving) {
				amt = currentNode.getRelativePosition(travelTime, nextNode);
			} 

			if (amt >= 1) {
				gameObject.transform.position = nextNode.transform.position;
				ArriveAt();
			} else {
				Vector3 nextPoint;
				if (nextNode.arc) {
					nextPoint = nextNode.getSlerp(currentNode.transform.position, amt);
				} else {
					nextPoint = Vector2.Lerp (currentNode.transform.position, nextNode.transform.position, amt);
				}
			
				if (directionMethod == DirectionMethod.ABSOLUTE) {
					
					gameObject.transform.eulerAngles =  direction;
				} else if (directionMethod == DirectionMethod.RELATIVE) {
					Vector3 heading = nextPoint - gameObject.transform.position;
					float deltaX =  heading.x;
					float deltaY =  heading.y;
					float rad = Mathf.Atan2(deltaX, deltaY); // In radians
					float deg = rad * GlobalDefines.globalDefines.RadToDeg;
					float angle = -1*deg + direction.z ;
					if (angle >= 360) angle -= 360;
					if (angle < 0) angle += 360;
					Vector3 newDirection = new Vector3(0f, 0f, angle);


					gameObject.transform.eulerAngles = newDirection; 
				} else if (directionMethod == DirectionMethod.POSITION) { 
				//	
					Vector3 relativeTarget = direction ;
					Vector3 relativePosition = (gameObject.transform.position - currentNode.transform.parent.position) ;
					//Debug.Log ( " Parent: " + currentNode.transform.parent.position.ToString () + " Target: " + direction.ToString() + " Relative Target: " + relativeTarget.ToString() + " Position: " + relativePosition.ToString());
					Vector3 heading = relativeTarget - relativePosition;
					float deltaX =  heading.x;
					float deltaY =  heading.y;
					float rad = Mathf.Atan2(deltaX, deltaY); // In radians
					float deg = rad * GlobalDefines.globalDefines.RadToDeg;
					float angle = -1*deg;
					if (angle >= 360) angle -= 360;
					if (angle < 0) angle += 360;
					Vector3 newDirection = new Vector3(0f, 0f, angle);
					
					
					gameObject.transform.eulerAngles = newDirection; 
				}

				gameObject.transform.position = nextPoint;

			}



		} else {
			if (currentNode.PerformStaticBehaviour(travelTime, this)) {
				travelTime = 0;
				behaviourExecuted = true;
			}

		}
	}

	void ArriveAt() {

		currentNode = nextNode;
		nextNode = nextNode.getNextNode(path);
		if (nextNode == null) {
			Object.Destroy(this.gameObject);
		}
		travelTime = 0;
		behaviourExecuted = false;


		if (currentNode.directionMethod != DirectionMethod.UNCHANGED) {
			direction = currentNode.direction;
			directionMethod = currentNode.directionMethod;
		}

	}

	public void setAspect(string val) {

		SpriteRenderer sprite = gameObject.GetComponent<SpriteRenderer>() as SpriteRenderer;

		foreach (char c in val) {
			if (c == 'r') {
				sprite.color = Color.red;

			} else if (c == 'g') {
				sprite.color = Color.green;
				
			} else if (c == 'b') {
				sprite.color = Color.blue;
				
			}

		}
	}


}
