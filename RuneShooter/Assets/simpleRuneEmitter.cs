﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class simpleRuneEmitter : waypointNode {

	public int desiredCount = -1; //the number of runes this emits. -1 = infinite.
	public float startTime = 0;	//The time after pattern start this starts emitting
	public float interval = 1f;	//The interval of time between run emissions.
	public bool emitsPatterns = false;
	public SerializablePattern patternToEmit;

	public int count = -1;
	int pathindex = 0;
	public List<int> pathscript = new List<int>();
	int aspectindex = 0;
	public List<string> aspectscript = new List<string>();

	int runesmade =0;
	float lastemit = 0f;

	List<GameObject> runes = new List<GameObject>(); //Emitters are responsible for cleaning up their own runes.

	public void DeleteEmitter() {
		Cleanup ();
		Delete ();
	}

	public SerializableEmitter SerializeEmitter() {
		SerializableEmitter se = new SerializableEmitter();
		se.pathscript = new List<int>();
		se.aspectscript = new List<string>();

		se.node = this.SerializeNode();
		se.desiredCount = desiredCount;

		se.startTime = startTime;
		se.interval = interval;

		se.emitsPatterns = emitsPatterns;
		se.patternToEmit = patternToEmit;
		foreach(int path in pathscript) {
			se.pathscript.Add (path);
		}
		foreach(string aspect in aspectscript) {
			se.aspectscript.Add (aspect);
		}

		return se;

	}
	public void DeserializeEmitter(SerializableEmitter se) {

		DeserializeNode(se.node);
		desiredCount = se.desiredCount;
		count = desiredCount;
		startTime = se.startTime;
		lastemit = -1f;
		interval = se.interval;
		emitsPatterns = se.emitsPatterns;
		patternToEmit = se.patternToEmit;

		foreach(int path in se.pathscript) {
			pathscript.Add (path);
		}
		foreach(string aspect in se.aspectscript) {
			aspectscript.Add (aspect);
		}

	}
	void Start() {
		if (GlobalDefines.globalDefines == null) {
			Debug.Log ("Warning, GlobalDefines not found");
		}
	}
	
	public void UpdateTick (float time) {

		float amtIn = time - lastemit;
		if (time < startTime) {
			return;
		}

		if (amtIn >= interval || lastemit == -1) {
			if (count > 0 || count == -1) {
				GameObject rune  = null;
				if (!emitsPatterns){
					rune = GameObject.Instantiate(GlobalDefines.globalDefines.SimpleRuneType);
					if (GlobalDefines.globalDefines.scaleToEditor) {
						rune.gameObject.transform.localScale *= GlobalDefines.globalDefines.editorScale;
					}
				} else {
					rune = GameObject.Instantiate(GlobalDefines.globalDefines.SimplePatternType);
					rune.layer = 8;
				}
				rune.transform.position = gameObject.transform.position;
				rune.transform.parent = gameObject.transform;
				simpleRuneMovement runeScript = rune.AddComponent<simpleRuneMovement>();

				if (pathscript.Count > pathindex) {
					runeScript.path = pathscript[pathindex];
	
					pathindex++;
					if (pathindex >= pathscript.Count) pathindex = 0;
				}
				
				if (aspectscript.Count > aspectindex) {
					runeScript.setAspect(aspectscript[aspectindex]);
					
					aspectindex++;
					if (aspectindex >= aspectscript.Count) aspectindex = 0;
				}


				runeScript.currentNode = this;
				runeScript.nextNode = this.getNextNode(runeScript.path);
				runesmade++;
				runes.Add(rune);
				lastemit = time;

				if (count != -1) {
					count--;
				}
				if (emitsPatterns) {

					runePattern pat = rune.AddComponent<runePattern>();

					pat.DeserializePattern(patternToEmit);

					rune.GetComponent<runePattern>().BeginPattern(Time.time);
					rune.GetComponent<runePattern>().name = "Spawned";
				} 
			}
		}
	}

	public void Cleanup() {
		foreach (GameObject obj in runes) {
			if (obj != null) {
				Destroy(obj);
			}
		}
		runes.Clear ();
		lastemit = -1f;
		count = desiredCount;
		aspectindex = 0;
		pathindex = 0;
	}

}


[Serializable]
public struct SerializableEmitter
{
	public SerializableWaypointNode node;
	public int desiredCount;
	public float startTime;
	public float interval;
	//TODO: This is recursive and can cause problems. 
	public SerializablePattern patternToEmit;
	public bool emitsPatterns;

	public List<int> pathscript;
	public List<string> aspectscript;
	
}
