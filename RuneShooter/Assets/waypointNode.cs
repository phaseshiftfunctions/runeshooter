﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;



public class waypointNode : MonoBehaviour {
	protected List<waypointNode> nextNode = new List<waypointNode>(); //next node
	protected List<waypointNode> lastNode = new List<waypointNode>(); //next node
	public bool arc = false; //slerp or lerp
	public bool locked = false;
	public Vector3 arcCenter;

	public bool canLink = true;

	public Vector3 direction = new Vector3(0f, 0f, 0f);
	public simpleRuneMovement.DirectionMethod directionMethod =  simpleRuneMovement.DirectionMethod.UNCHANGED;


	Dictionary<int, float> times = new Dictionary<int, float>(); //The time it takes to get to the next node (based on instance id.
	baseNodeBehaviour staticbehaviour = null;
	baseNodeBehaviour movingbehaviour = null;
	bool overridetraveltime = false;
	
	Dictionary<int, GameObject> lines = new Dictionary<int, GameObject>(); //Game objects that own the line render for each previous node.
	GameObject directionLine = null;

	public SerializableWaypointNode SerializeNode() {
		SerializableWaypointNode sn = new SerializableWaypointNode();
		sn.nodeID = this.gameObject.GetInstanceID();
		sn.nextNode = new List<int>();
		sn.nextNodeTimes = new List<float>();
		sn.lastNode = new List<int>();
		if (GlobalDefines.globalDefines.scaleToEditor) {
			sn.position = gameObject.transform.position / GlobalDefines.globalDefines.editorScale;
		} else {
			sn.position = gameObject.transform.position;
		}

		foreach(waypointNode node in nextNode) {

			sn.nextNode.Add (node.gameObject.GetInstanceID());
			sn.nextNodeTimes.Add (times[node.gameObject.GetInstanceID()]);

		}
		foreach(waypointNode node in lastNode) {
			sn.lastNode.Add (node.gameObject.GetInstanceID());
		}
		sn.arcCenter = arcCenter;
		sn.arc = arc;
		sn.directionMethod = directionMethod;
		if (directionMethod == simpleRuneMovement.DirectionMethod.POSITION && GlobalDefines.globalDefines.scaleToEditor) {
			sn.direction = direction / GlobalDefines.globalDefines.editorScale;
		} else {
			sn.direction = direction;
		}



		return sn;
	}
	public void DeserializeNode(SerializableWaypointNode sn) {

		arcCenter = sn.arcCenter;
		arc = sn.arc;

		directionMethod = sn.directionMethod;
		direction = sn.direction;
		if (directionMethod == simpleRuneMovement.DirectionMethod.POSITION && GlobalDefines.globalDefines.scaleToEditor) {
			direction = direction * GlobalDefines.globalDefines.editorScale;
		} 
		Debug.Log (direction.ToString());
		gameObject.transform.position = sn.position;
		if (GlobalDefines.globalDefines.scaleToEditor) {
			gameObject.transform.position *= GlobalDefines.globalDefines.editorScale;
		} 
		gameObject.transform.position += gameObject.transform.parent.position;
	}
	public void Delete() {
		foreach (int key in lines.Keys) {
			GameObject obj = lines[key];
			GameObject.Destroy (obj);
		}
		lines.Clear ();
		foreach (waypointNode node in lastNode) {
			node.removeNextNode(this.GetInstanceID());
		}
		foreach (waypointNode node in nextNode) {
			node.removeLastNode(this.GetInstanceID());
			node.RenderLine();
		}
		Destroy (gameObject);
	}
	void Update() {
		RenderLine();
	}
	public baseNodeBehaviour GetStaticBehaviour() {
		return staticbehaviour;
	}
	public void RemoveStaticBehaviour() {

		Destroy (staticbehaviour);
		staticbehaviour = null;

	}
	public void ApplyStaticBehaviour(baseNodeBehaviour beh) {
		staticbehaviour = beh;


	}
	public void ApplyMovingBehaviour(baseNodeBehaviour beh) {
		movingbehaviour = beh;
	}
	public bool PerformStaticBehaviour(float time, simpleRuneMovement rune) {
		if (staticbehaviour == null) {
			return true;
		} 
		return staticbehaviour.performBehaviours(time, rune);
	}
	public bool PerformMovingBehaviour(float time, simpleRuneMovement rune) {
		if (movingbehaviour == null) {
			return true;
		} 
		return movingbehaviour.performBehaviours(time, rune);
	}

	public void removeNextNode(int id) {
		foreach (waypointNode node in nextNode) {
			if (node.GetInstanceID() == id) {
				nextNode.Remove(node);
				times.Remove (node.gameObject.GetInstanceID());
				return;
			}
		}
	}
	public void removeLastNode(int id) {
		if (lines.ContainsKey(id)) {
			GameObject obj = lines[id];
			lines.Remove(id);
			GameObject.Destroy(obj);
		}
		foreach (waypointNode node in lastNode) {
			if (node.GetInstanceID() == id) {
				lastNode.Remove(node);
				return;
			}
		}
	}

	public float getRelativePosition(float time, waypointNode node) {
		if (times.ContainsKey(node.gameObject.GetInstanceID())) {
			return time / times[node.gameObject.GetInstanceID()];
		}
		return 1;

	}

	public waypointNode getNextNode(int index) {
		if (index < nextNode.Count && index >= 0) {
			return nextNode[index];
		}
		if (nextNode.Count > 0) {
			return nextNode[0];
		}
		return null;
	}

	
	public void addNextNode(waypointNode node) {
		nextNode.Add (node);
		if (!times.ContainsKey(node.gameObject.GetInstanceID())) {
			times[node.gameObject.GetInstanceID()] = 1.0f;
		}

	}
	public void addLastNode(waypointNode node) {
		lastNode.Add (node);
	}

	public int getLastNodeCount() {
		return lastNode.Count;
	}
	public List<waypointNode> getLastNodeList() {
		return lastNode;
	}	
	public List<waypointNode> getNextNodeList() {
		return nextNode;
	}
	public int getNextNodeCount() {
		return nextNode.Count;
	}

	public void setTravelTime(int instance, float time, bool over) {
		if (overridetraveltime) {
			if (!over) {
				return;
			}
		}
		if (over) overridetraveltime = over;

		if (time == -1) {
			overridetraveltime = false;
		}
		this.times[instance] = time;

	}
	public float getTravelTime(int instance) {
		return this.times[instance];
	}
	public void RenderLine() {
		foreach (waypointNode node in lastNode) {
			this.PositionLine(node);
		}
		foreach (waypointNode node in nextNode) {
			node.PositionLine(this);
		}
		this.PositionDirectionLine();

	}
	public Vector3 getSlerp(Vector3 pos, float amt) {

		Vector3 pos1 = this.gameObject.transform.position - this.gameObject.transform.parent.position ;

		Vector3 pos2 = pos - this.gameObject.transform.parent.position;;

		return (Vector3.Slerp(pos2 - arcCenter, pos1 - arcCenter, amt) + arcCenter + gameObject.transform.parent.position);


	}
	public void PositionDirectionLine() {
		if (directionLine == null) {
			directionLine = this.CreateLineRenderer();
			directionLine.layer = 8;
		}
		directionLine.transform.position = this.gameObject.transform.position;
		directionLine.transform.parent = this.gameObject.transform;
		LineRenderer thisline = directionLine.GetComponent<LineRenderer>();
		thisline.material = GlobalDefines.globalDefines.LineRendererMaterial;

		if (directionMethod == simpleRuneMovement.DirectionMethod.ABSOLUTE) {
			thisline.SetColors(Color.red, Color.red);
			float x = Mathf.Cos((direction.z + 90f)  * (Mathf.PI/180));
			float y = Mathf.Sin((direction.z + 90f)  * (Mathf.PI/180));
			Vector3 newPos = new Vector3(x,y,0f);
			Debug.Log( direction + "  " + x + "   " + y);
			thisline.SetVertexCount(2);
			
			thisline.SetPosition(0, this.gameObject.transform.position);
			thisline.SetPosition(1, this.gameObject.transform.position + newPos*2);

		} else if (directionMethod == simpleRuneMovement.DirectionMethod.RELATIVE){
			thisline.SetColors(Color.cyan, Color.green);
			float x = Mathf.Cos((direction.z + 90f) * (Mathf.PI/180));
			float y = Mathf.Sin((direction.z + 90f) * (Mathf.PI/180));
			Vector3 newPos = new Vector3(x,y,0f);
			
			thisline.SetVertexCount(3);
			

			thisline.SetPosition(0, this.gameObject.transform.position + newPos*2);
			thisline.SetPosition(1, this.gameObject.transform.position);
			thisline.SetPosition(2, this.gameObject.transform.position + new Vector3(0f,1f,0f));
		} else if (directionMethod == simpleRuneMovement.DirectionMethod.POSITION){
			thisline.SetColors(Color.gray, Color.gray);
			
			thisline.SetVertexCount(2);
			thisline.SetPosition(0, this.gameObject.transform.position);
			thisline.SetPosition(1, direction + gameObject.transform.parent.position);
			
		} else if (directionMethod == simpleRuneMovement.DirectionMethod.UNCHANGED) {
			thisline.SetVertexCount(0);
		}
	}

	public void PositionLine(waypointNode node) {
		bool render = false;
		GameObject lineObject;

		if (lines.ContainsKey(node.GetInstanceID())) {
			lineObject = lines[node.GetInstanceID()];
		} else {
			lineObject = this.CreateLineRenderer();
			lineObject.layer = 8;

			lines[node.GetInstanceID()]= lineObject;

		}
		lineObject.transform.position = this.gameObject.transform.position;
		lineObject.transform.parent = this.gameObject.transform;
		LineRenderer thisline =lineObject.GetComponent<LineRenderer>();
		thisline.SetColors(Color.green, Color.yellow);
		thisline.material = GlobalDefines.globalDefines.LineRendererMaterial;
		thisline.SetVertexCount(0);

		if (arc) {
			int steps = 10;
			thisline.SetVertexCount(steps);

			for (int x = 0; x < steps; x++) {
				thisline.SetPosition(x, getSlerp(node.gameObject.transform.position, (float)x/(float)steps));
			}
		} else {
			thisline.SetVertexCount(2);

			thisline.SetPosition(0, this.gameObject.transform.position);
			thisline.SetPosition(1, node.gameObject.transform.position);
		}
		render = true;
		thisline.enabled = render;



	}

	GameObject CreateLineRenderer() {
		GameObject newObject= new GameObject();
		newObject.transform.position = this.gameObject.transform.position;
		LineRenderer newLine = newObject.AddComponent<LineRenderer>();
		newLine.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		newLine.receiveShadows = false;
		newLine.material = GlobalDefines.globalDefines.LineRendererMaterial;
		newLine.useLightProbes = false;
		newLine.SetWidth(0.5f, 0.5f);
		newLine.SetColors(Color.cyan, Color.red);
		return newObject;
	}


}

[Serializable]
public struct SerializableWaypointNode
{
	public int nodeID; //This will be the instance ID of the unity object. Persisting it doesn't matter, its only used to populate the lists. 
	public List<int> nextNode;
	public List<float> nextNodeTimes;
	public List<int> lastNode;
	public SerializableVector3 arcCenter;
	public SerializableVector3 position;
	public SerializableVector3 direction;
	public simpleRuneMovement.DirectionMethod directionMethod;
	public bool arc;

	
	
}

