﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SimpleLevel : MonoBehaviour {

	float startTime;
	float currentTime;

	SerializableLevel level;
	bool started = false;



	float duration = 600f;
	Dictionary<int, SerializableLevelTimings> timings = new Dictionary<int, SerializableLevelTimings>();
	List<runePattern> activePatterns = new List<runePattern>();
	List<int> order = new List<int>();	//This list contains the order that we place patterns in.
	int currentIndex = 0;

	void Update () {
		if (started) {
			currentTime += Time.deltaTime * GlobalDefines.globalDefines.timeRate;
		
			while (timings.ContainsKey(currentIndex) && currentTime >= timings[currentIndex].startTime){

				GameObject patternGO = GameObject.Instantiate(GlobalDefines.globalDefines.SimplePatternType);
				patternGO.layer = 8;
				runePattern pattern = patternGO.AddComponent<runePattern>();
				
				pattern.DeserializePattern(timings[currentIndex].pattern);
				pattern.BeginPattern(currentTime);
				activePatterns.Add(pattern);
				currentIndex++;
			}
		}
	}

	public void StartLevel() {
		startTime = Time.time;
		started = true;
	}
	public void StopLevel() {
		started = false;
		foreach(runePattern pattern in activePatterns) {
			pattern.EndPattern();
			Destroy(pattern);
		}
		activePatterns.Clear ();
		currentIndex = 0;
	}
	public void SetTime(float time) {
		currentTime = time;
	}

	public float GetTime() {
		return currentTime;
	}
	public bool IsStarted() {
		return started;
	}
	public void SetDuration(float time) {
		duration = time;
	}

	public float GetDuration() {

		return duration;
	}
	public void UpdatePatternTime(int index, float time) {
		if (!timings.ContainsKey(index)) {
			return;
		}
		SerializableLevelTimings set = timings[index];
		set.startTime = time;
		timings[index] = set;
		bool found = false;
		order.Remove(index);
		for (int x = 0; x < order.Count; x++) {
			if (time > timings[order[x]].startTime) {
				order.Insert(x+1, index);
				x = order.Count;
				found = true;
			}
			
		}
		if (!found) {
			order.Insert(0, index);
		}

	}


	public int AddPattern(SerializablePattern pattern, float time) {
		SerializableLevelTimings Set;
		Set.pattern = pattern;
		Set.startTime = time;
		int index = 0;
		while (timings.ContainsKey(index)) {
			index++;
		}
		Set.id = index;
		timings[index] = Set;
		bool found = false;
		for (int x = 0; x < order.Count; x++) {
			if (time > timings[order[x]].startTime) {
				order.Insert(x+1, index);
				x = order.Count;
				found = true;
			}
			 
		}
		if (!found) {
			order.Insert(0, index);
		}

		return index;
	}
	public void DeletePattern(int index) {
		timings.Remove (index);
		order.Remove(index);

	}
	public SerializableLevelTimings GetPatternSet(int index) {

		return timings[index];
	}
	public int GetCurrentIndex() {
		return currentIndex;
	}
	public int GetOrderCount() {
		return order.Count;
	}
	public int GetOrder(int index) {
		return order[index];
	}
	public int GetIndexOfOrder(int index) {
		return order.IndexOf(index);
	}
}



[Serializable]
public struct SerializableLevel
{
	public List<SerializableLevelTimings> levelsAndTimings;
}

[Serializable]
public struct SerializableLevelTimings
{
	public int id;
	public float startTime;
	public SerializablePattern pattern;
}